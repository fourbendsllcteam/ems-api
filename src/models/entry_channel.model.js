const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { indexes } = require('../config/secondaryindex');
const logger = require('../config/logger');
const AWS = require('aws-sdk');
const { awsConfig } = require('../config/config');

AWS.config = awsConfig;
const docClient = new AWS.DynamoDB.DocumentClient();

const PKPrefixEntryChannel = 'ENTRY_CHANNEL_';

const tableName = 'ems_model';

const createEntryChannelEmployeeImpl = async (data) => {
  logger.debug('inside createEntryChannelEmployeeImpl data entry: ', JSON.stringify(data));
  var params = {
    TableName: tableName,
    Item: {
      pk: PKPrefixEntryChannel + data.channelName,
      sk: data.entryChannelEmpId,
      channel_name: data.channelName,
      channel_id: data.channelId,
      emp_id: data.entryChannelEmpId,
    },
  };
  logger.info('Adding a entry channel...');
  await docClient
    .put(params, function (err) {
      if (err) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Error in saving data. Error: ' + JSON.stringify(err, null, 2));
      }
    })
    .promise();

  return params;
};

const deleteEntryChannelImpl = async (channelName, empId) => {
  var params = {
    TableName: tableName,
    Key: {
      pk: PKPrefixEntryChannel + channelName,
      sk: empId,
    },
  };
  logger.debug('deleting entry channel: ', channelName);
  await docClient
    .delete(params, function (err) {
      if (err) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, JSON.stringify(err, null, 2));
      }
    })
    .promise();
};

module.exports = {
  createEntryChannelEmployeeImpl,
  deleteEntryChannelImpl,
};

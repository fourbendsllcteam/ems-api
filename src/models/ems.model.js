/* eslint-disable no-console */
const dynamoose = require('dynamoose');
const { has } = require('express-mongo-sanitize');

const adminUserAttributes = ['username', 'password', 'user_email', 'name', 'roles'];

const attendanceAttributes = [
  'company_emp_id',
  'exit_channel',
  'lacked_hours',
  'shift_name',
  'emp_name',
  'emp_id',
  'first_in',
  'last_out',
  'exit_logs',
  'ot_hours',
  'entry_logs',
  'duration',
  'entry_channel',
  'current_status',
  'dept_name',
  'attn_date',
  'period_attendance',
  'attn_reg_status',
  'ideal_duration',
];

const tokenAttributes = ['token', 'expires', 'blacklisted', 'type', 'user'];

const employeeAttributes = [
  'company_emp_id',
  'emp_id',
  'emp_name',
  'first_name',
  'last_name',
  'address',
  'email',
  'phone',
  'emp_type',
  'department',
  'shift',
  'entry_channel',
  'current_day',
  'search_names',
];

const emsSchema = new dynamoose.Schema(
  {
    pk: {
      type: String,
      hashKey: true,
    },
    sk: {
      type: String,
      rangeKey: true,
      index: {
        name: 'inverted-index',
        hashKey: true,
        rangeKey: 'pk',
        global: 'true',
        project: true,
      },
    },
    emp_id: {
      type: String,
    },
    department: {
      type: String,
      index: {
        name: 'department-employee',
        hashKey: true,
        rangeKey: 'emp_id',
        global: 'true',
        project: employeeAttributes,
      },
    },
    shift: {
      type: String,
      index: {
        name: 'shift-employee',
        hashKey: true,
        rangeKey: 'emp_id',
        global: 'true',
        project: employeeAttributes,
      },
    },
    company_emp_id: {
      type: String,
      index: {
        name: 'id-employee',
        hashKey: true,
        rangeKey: 'emp_id',
        global: 'true',
        project: employeeAttributes,
      },
    },
    user_email: {
      type: String,
    },
    username: {
      type: String,
      index: {
        name: 'user-index',
        hashKey: true,
        rangeKey: false,
        global: true,
        project: adminUserAttributes,
      },
    },
    token: {
      type: String,
      index: {
        name: 'token-index',
        hashKey: true,
        rangeKey: false,
        global: true,
        project: tokenAttributes,
      },
    },
    attn_date: {
      type: String,
    },
    period_attendance: {
      type: String,
      index: {
        hashKey: true,
        name: 'period-attendance',
        rangeKey: 'attn_date',
        global: true,
        project: attendanceAttributes,
      },
    },
    current_status: {
      type: String,
      index: {
        hashKey: 'true',
        name: 'status-attendance',
        rangeKey: false,
        global: true,
        project: attendanceAttributes,
      },
    },
    ot_hours: {
      type: Number,
    },

    channel_id: {
      type: String,
    },
    channel_name: {
      type: String,
      index: {
        hashKey: true,
        rangeKey: 'channel_id',
        name: 'channel-employee',
        global: 'true',
        project: ['channel_id', 'channel_name', 'emp_id'],
      },
    },
  },

  {
    saveUnknown: true,
  }
);

module.exports = dynamoose.model('ems_model', emsSchema);

const logger = require('../config/logger');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { indexes } = require('../config/secondaryindex');

const AWS = require('aws-sdk');
const { awsConfig } = require('../config/config');

AWS.config = awsConfig;
const docClient = new AWS.DynamoDB.DocumentClient();

const tableName = 'ems_model';
const IndexPKPeriodAttendance = 'PERIOD_ATTN';

const getEmployeesCountByStatusImpl = async (status) => {
  logger.info('inside getCurrentlyAvailableEmployeesCount: ');
  let count = 0;
  let params = {
    TableName: tableName,
    IndexName: indexes.STATUS_ATTENDANCE,
    KeyConditionExpression: 'current_status = :status',
    ExpressionAttributeValues: {
      ':status': status,
    },
  };
  let result = await docClient.query(params).promise();
  return result.Count;
};

const getOTEmployeesCountImpl = async () => {
  logger.info('inside getOTEmployeesCountImpl: ');
  //let result = 0;
  var params = {
    TableName: tableName,
    FilterExpression: 'attn_reg_status = :attn_status and ot_hours > :ot_hours',

    ExpressionAttributeValues: {
      ':attn_status': 'OPEN',
      ':ot_hours': 0,
    },
  };

  let result = await docClient.scan(params).promise();
  logger.debug('resu', result.Count);
  return result.Count;
};

module.exports = {
  getEmployeesCountByStatusImpl,
  getOTEmployeesCountImpl,
};

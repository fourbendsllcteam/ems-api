const logger = require('../config/logger');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const employeeService = require('../services/employee.service');
const { v4: uuidv4 } = require('uuid');
const { indexes } = require('../config/secondaryindex');
const entryChannelModel = require('./entry_channel.model');
const AWS = require('aws-sdk');
const { awsConfig } = require('../config/config');

AWS.config = awsConfig;
const docClient = new AWS.DynamoDB.DocumentClient();

const PKEmployeeMaster = 'EMP_PROF_MASTER';
const SKPrefixEmployeeMaster = '_EMP_PROF_';

const tableName = 'ems_model';

const getEmployeeByIdImpl = async (id) => {
  let params = {
    TableName: tableName,
    Key: {
      pk: PKEmployeeMaster,
      sk: SKPrefixEmployeeMaster + id,
    },
  };
  let employee = await docClient.get(params).promise();
  return employee.Item;
};

const deleteEmployeeImpl = async (empId) => {
  const employee = await getEmployeeByIdImpl(empId);
  if (!employee) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
  const entryChannels = employee.entry_channel;
  logger.debug('emp has entry channels:', JSON.stringify(entryChannels));

  for (key in entryChannels) {
    if (!entryChannels[key] == '') {
      logger.debug('this entry channel has not null value: ', key);
      await entryChannelModel.deleteEntryChannelImpl(key, empId);
    }
  }
  var params = {
    TableName: tableName,
    Key: {
      pk: PKEmployeeMaster,
      sk: SKPrefixEmployeeMaster + empId,
    },
  };
  await docClient
    .delete(params, function (err) {
      if (err) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, JSON.stringify(err, null, 2));
      }
    })
    .promise();
};

const updateShiftByDepartmentImpl = async (departmentName, reqBody) => {
  const employees = await getEmployeesByDepartment(departmentName);
  let result = '';
  let data = { shift: reqBody.shift };
  if (employees.length >= 0) {
    for (const emp of employees) {
      result = await updateEmployee(emp.emp_id, data);
    }
  } else {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
};

const updateEntryChannelImpl = async (empId, entryChannel) => {
  var params = {
    TableName: tableName,
    Key: {
      pk: PKEmployeeMaster,
      sk: SKPrefixEmployeeMaster + empId,
    },
    UpdateExpression: 'set entry_channel = :entry_channel',
    ExpressionAttributeValues: {
      ':entry_channel': entryChannel,
    },
    ReturnValues: 'UPDATED_NEW',
  };

  logger.info('Updating the item...');
  let result = await docClient
    .update(params, function (err, data) {
      if (err) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, JSON.stringify(err, null, 2));
      }
    })
    .promise();
};

const updateEmployeeImpl = async (empId, data) => {
  logger.debug('inside updateEmployeeImpl: ', JSON.stringify(data));

  const employee = await getEmployeeByIdImpl(empId);
  if (!employee) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
  let newSearchNames = employee.search_names;
  if (!data.firstName === null || !data.lastName === null) {
    logger.info('fn & ln not null');
    newSearchNames = (data.firstName.toLowerCase() + data.lastName.toLowerCase()).replace(/\s+/g, '');
  }

  var params = {
    TableName: tableName,
    Key: {
      pk: PKEmployeeMaster,
      sk: SKPrefixEmployeeMaster + empId,
    },
    UpdateExpression:
      'set first_name = :fn, last_name = :ln, address=:add, email = :email, phone=:phone, emp_type=:type, current_day = :currentDay, search_names=:searchNames,shift=:shift,department=:dept,entry_channel = :entryChannel',
    ExpressionAttributeValues: {
      ':fn': data.firstName || employee.first_name,
      ':ln': data.lastName || employee.last_name,
      ':add': data.address || employee.address,
      ':email': data.email || employee.email,
      ':phone': data.phone || employee.phone,
      ':type': data.empType || employee.emp_type,
      ':currentDay': data.currentDay || employee.current_day || '',
      ':searchNames': newSearchNames,
      ':shift': data.shift || employee.shift,
      ':dept': data.department || employee.department,
      ':entryChannel': data.entryChannel || employee.entry_channel,
    },
    ReturnValues: 'UPDATED_NEW',
  };

  logger.info('Updating the item...');
  let result = await docClient
    .update(params, function (err, data) {
      if (err) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, JSON.stringify(err, null, 2));
      }
    })
    .promise();
};

const getEmployeesByNameImpl = async (param) => {
  let params = {
    TableName: tableName,
    KeyConditionExpression: '#pk = :pk',
    FilterExpression: 'contains (search_names, :sub) ',
    ExpressionAttributeNames: {
      '#pk': 'pk',
    },
    ExpressionAttributeValues: {
      ':pk': PKEmployeeMaster,
      ':sub': param,
    },
  };
  let employees = await docClient.query(params).promise();
  return employees.Items;
};

const getEmployeesByDepartmentImpl = async (param) => {
  let params = {
    TableName: tableName,
    IndexName: indexes.DEPARTMENT_EMPLOYEE,
    KeyConditionExpression: '#department = :dept',
    ExpressionAttributeNames: {
      '#department': 'department',
    },
    ExpressionAttributeValues: {
      ':dept': param,
    },
  };
  let employees = await docClient.query(params).promise();
  return employees.Items;
};

const getEmployeesByCompanyEmpIdImpl = async (companyEmpId) => {
  logger.info('inside getEmployeesByCompanyEmpIdImpl ');
  let params = {
    TableName: tableName,
    IndexName: indexes.COMPANY_EMP_ID_INDEX,
    KeyConditionExpression: 'company_emp_id = :id',
    ExpressionAttributeValues: {
      ':id': companyEmpId,
    },
  };
  let employees = await docClient.query(params).promise();
  return employees.Items;
};

const getEmployeesByShiftImpl = async (param) => {
  let params = {
    TableName: tableName,
    IndexName: indexes.SHIFT_EMPLOYEE,
    KeyConditionExpression: '#shift = :shift',
    ExpressionAttributeNames: {
      '#shift': 'shift',
    },
    ExpressionAttributeValues: {
      ':shift': param,
    },
  };
  let employees = await docClient.query(params).promise();
  return employees.Items;
};

const getEmployeeByChannelImpl = async (channelName, channelId) => {
  // logger.debug('test: ' + channelName + ' | ' + channelId);
  let params = {
    TableName: tableName,
    IndexName: indexes.CHANNEL_EMPLOYEE,
    KeyConditionExpression: 'channel_name = :channel_name and channel_id = :channel_id',
    ExpressionAttributeValues: {
      ':channel_name': channelName,
      ':channel_id': channelId,
    },
  };
  let employees = await docClient.query(params).promise();
  return employees.Items;
};

const getAllEmployeesImpl = async () => {
  let params = {
    TableName: tableName,
    KeyConditionExpression: '#pk = :pk',
    ExpressionAttributeNames: {
      '#pk': 'pk',
    },
    ExpressionAttributeValues: {
      ':pk': PKEmployeeMaster,
    },
  };
  let employees = await docClient.query(params).promise();
  return employees.Items;
};

const createEmployeeImpl = async (data) => {
  const emp_uuid = uuidv4();
  var params = {
    TableName: tableName,
    Item: {
      pk: PKEmployeeMaster,
      sk: SKPrefixEmployeeMaster + emp_uuid,
      company_emp_id: data.companyEmpId,
      emp_id: emp_uuid,
      first_name: data.firstName,
      last_name: data.lastName,
      address: data.address,
      email: data.email,
      phone: data.phone,
      emp_type: data.empType,
      department: data.department,
      shift: data.shift,
      current_day: '',
      search_names: (data.firstName.toLowerCase() + data.lastName.toLowerCase()).replace(/\s+/g, ''),
      entry_channel: data.entryChannel,
    },
  };
  logger.info('Adding a employee...');
  let result = await docClient
    .put(params, function (err) {
      if (err) {
        throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Error in saving data. Error: ' + JSON.stringify(err, null, 2));
      }
    })
    .promise();

  return params;
};

module.exports = {
  createEmployeeImpl,
  getAllEmployeesImpl,
  updateEmployeeImpl,
  getEmployeeByIdImpl,
  deleteEmployeeImpl,
  getEmployeesByNameImpl,
  getEmployeesByDepartmentImpl,
  getEmployeesByShiftImpl,
  getEmployeeByChannelImpl,
  updateShiftByDepartmentImpl,
  updateEntryChannelImpl,
  getEmployeesByCompanyEmpIdImpl,
};

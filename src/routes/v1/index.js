const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const docsRoute = require('./docs.route');
const empRoute = require('./employee.route');
const shiftRoute = require('./shift.route');
const departmentRoute = require('./department.route');
const attendanceRoute = require('./attendance.route');
const dashboardRoute = require('./dashboard.route');

const router = express.Router();

router.use('/auth', authRoute);
router.use('/users', userRoute);
router.use('/docs', docsRoute);
router.use('/departments', departmentRoute);
router.use('/shifts', shiftRoute);
router.use('/employees', empRoute);
router.use('/attendance', attendanceRoute);
router.use('/dashboard', dashboardRoute);

module.exports = router;

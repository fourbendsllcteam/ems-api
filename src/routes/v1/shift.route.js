/* eslint-disable prettier/prettier */
const express = require('express');

const validate = require('../../middlewares/validate');
const shiftValidation = require('../../validations/shift.validation');
const shiftController = require('../../controllers/shift.controller');
const auth = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/')
  .post(auth('manageShift'), validate(shiftValidation.createShift), shiftController.createShift)
  .get(shiftController.getAllShifts);

router
  .route('/:id')
  .patch(auth('manageShift'), validate(shiftValidation.updateShift), shiftController.updateShift)
  .delete(auth('manageShift'), validate(shiftValidation.deleteShift), shiftController.deleteShift);

module.exports = router;

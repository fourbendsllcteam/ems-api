/* eslint-disable prettier/prettier */
const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const departmentValidation = require('../../validations/department.validation');
const departmentController = require('../../controllers/department.controller');

const router = express.Router();

router
  .route('/')
  .post(auth('manageDepartment'), validate(departmentValidation.createDepartment), departmentController.createDepartment)
  .get(auth('manageDepartment'), departmentController.getAllDepartments);

router
  .route('/:id')
  .patch(auth('manageDepartment'), validate(departmentValidation.updateDepartment), departmentController.updateDepartment)
  .delete(auth('manageDepartment'), validate(departmentValidation.deleteDepartment), departmentController.deleteDepartment);

module.exports = router;

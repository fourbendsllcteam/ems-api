/* eslint-disable prettier/prettier */
const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const attendanceValidation = require('../../validations/attendance.validation');
const attendanceController = require('../../controllers/attendance.controller');

const router = express.Router();

router.route('/logging').post(validate(attendanceValidation.checkLogging), attendanceController.updateAttendanceRegister);

router.route('/:empId').get(auth('manageAttendance'), attendanceController.getEmployeeWiseAttendance);

router
  .route('/')
  .post(validate(attendanceValidation.createAttendanceRegister), attendanceController.createAttendanceRegister)
  .get(auth('manageAttendance'), attendanceController.getPeriodWiseAttendance);

module.exports = router;

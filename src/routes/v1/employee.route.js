/* eslint-disable prettier/prettier */
const express = require('express');

const validate = require('../../middlewares/validate');
const employeeValidation = require('../../validations/employee.validation');
const employeeController = require('../../controllers/employee.controller');
const auth = require('../../middlewares/auth');

const router = express.Router();
router.route('/update-shift').post(validate(employeeValidation.updateShift), employeeController.updateShift);

router
  .route('/')
  .post(auth('manageEmployees'), validate(employeeValidation.createEmployee), employeeController.createEmployee)
  .get(auth('manageEmployees'), employeeController.getAllEmployees);

router
  .route('/:id')
  .patch(auth('manageEmployees'), validate(employeeValidation.updateEmployee), employeeController.updateEmployee)
  .delete(auth('manageEmployees'), validate(employeeValidation.deleteEmployee), employeeController.deleteEmployee)
  .get(auth('manageEmployees'), validate(employeeValidation.getEmployee), employeeController.getEmployee);

module.exports = router;

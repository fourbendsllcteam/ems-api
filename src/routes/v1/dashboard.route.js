/* eslint-disable prettier/prettier */
const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const dashboardController = require('../../controllers/dashboard.controller');

const router = express.Router();

router.route('/').get(auth('manageDashboard'), dashboardController.getDashboardData);

router.route('/test-route-up').get(dashboardController.testRoute);
module.exports = router;

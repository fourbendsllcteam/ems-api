/* eslint-disable prettier/prettier */
/* eslint-disable no-empty */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const departmentService = require('../services/department.service');

const createDepartment = catchAsync(async (req, res) => {
  const department = await departmentService.createDept(req.body);
  res.status(httpStatus.CREATED).json({ status: 'success', data: { department } });
});

const getAllDepartments = catchAsync(async (req, res) => {
  const departments = await departmentService.getAllDepts();
  res.status(httpStatus.OK).json({
    status: 'success',
    result: departments.length,
    data: {
      departments,
    },
  });
});

const updateDepartment = catchAsync(async (req, res) => {
  let department = await departmentService.updateDept(req.params.id, req.body);
  department = await departmentService.getDeptById(req.params.id);
  res.status(httpStatus.OK).json({ status: 'success', data: { department } });
});

const deleteDepartment = catchAsync(async (req, res) => {
  await departmentService.deleteDept(req.params.id);
  res.status(httpStatus.OK).json({ status: 'success', message: 'Deleted success!' });
});

module.exports = {
  createDepartment,
  getAllDepartments,
  updateDepartment,
  deleteDepartment,
};

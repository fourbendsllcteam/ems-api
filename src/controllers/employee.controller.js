const ApiError = require('../utils/ApiError');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const employeeService = require('../services/employee.service');
const logger = require('../config/logger');

const createEmployee = catchAsync(async (req, res) => {
  logger.info('employee.controller.js - inside createEmployee');
  const employee = await employeeService.createEmployee(req.body);
  logger.debug('employee.controller.js - employee: ', employee);
  if (employee.Item) {
    res.status(httpStatus.CREATED).json({ status: 'success', data: { employee } });
  } else {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).json({ status: 'failure', message: 'Error in creating employee' });
  }
});

const getAllEmployees = catchAsync(async (req, res) => {
  let employees;
  if (req.query.name) {
    logger.debug('param, lenght', req.query.name);
    if (
      req.query.name === '' ||
      req.query.name === null ||
      req.query.name === undefined ||
      req.query.name.length < 3 ||
      req.query.name.length === 0
    ) {
      throw new ApiError(httpStatus.OK, 'Please enter atlease 3 characters');
    }
    employees = await employeeService.getEmployeesByName(req.query.name);
  } else if (req.query.department) {
    employees = await employeeService.getEmployeesByDepartment(req.query.department);
  } else if (req.query.shift) {
    logger.debug('employess by shift', req.query.shift);
    employees = await employeeService.getEmployeesByShift(req.query.shift);
  } else if (req.query.ts) {
    employees = await employeeService.getEmployeesOfCurrentShift(req.query.ts);
  } else {
    logger.info('inside getAll');

    employees = await employeeService.getAllEmployees();
  }
  logger.debug('emp', employees);
  res.status(httpStatus.OK).json({
    status: 'success',
    result: employees.length,
    data: {
      employees,
    },
  });
});

const getEmployee = catchAsync(async (req, res) => {
  const employee = await employeeService.getEmployeeByCompanyEmpId(req.params.id.toUpperCase());
  res.status(httpStatus.OK).json({
    status: 'success',
    data: {
      employee,
    },
  });
});

const updateEmployee = catchAsync(async (req, res) => {
  let employee;
  employee = await employeeService.updateEmployee(req.params.id, req.body);
  employee = await employeeService.getEmployee(req.params.id);
  res.status(httpStatus.OK).json({ status: 'success', data: { employee } });
});

const updateShift = catchAsync(async (req, res) => {
  const employees = req.body.employees;
  const departments = req.body.departments;
  let result;
  if (req.body.employees) {
    for (const empId of employees) {
      result = await employeeService.updateEmployee(empId, req.body);
    }
  }

  if (req.body.departments) {
    for (const dept of departments) {
      logger.debug('dept ' + dept);
      result = await employeeService.updateShiftByDepartment(dept, req.body);
    }
  }
  res.status(httpStatus.OK).json({ status: 'success', message: 'updation done' });
});

const deleteEmployee = catchAsync(async (req, res) => {
  await employeeService.deleteEmployee(req.params.id);
  res.status(httpStatus.OK).json({ status: 'success', message: 'Deleted success!' });
});

module.exports = {
  createEmployee,
  getAllEmployees,
  updateEmployee,
  deleteEmployee,
  getEmployee,
  updateShift,
};

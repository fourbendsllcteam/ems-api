/* eslint-disable prettier/prettier */
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const shiftService = require('../services/shift.service');

const createShift = catchAsync(async (req, res) => {
  const shift = await shiftService.createShift(req.body);
  // logger.debug('respnse: ', shift);
  res.status(httpStatus.CREATED).json({ status: 'success', data: { shift } });
});

const getAllShifts = catchAsync(async (req, res) => {
  const shifts = await shiftService.getAllShifts();

  res.status(httpStatus.OK).json({
    status: 'success',
    result: shifts.length,
    data: {
      shifts,
    },
  });
});

const updateShift = catchAsync(async (req, res) => {
  let shift = await shiftService.updateShift(req.params.id, req.body);
  shift = await shiftService.getShiftById(req.params.id);
  res.status(httpStatus.OK).json({ status: 'success', data: { shift } });
});

const deleteShift = catchAsync(async (req, res) => {
  logger.info('inside delete controller');
  await shiftService.deleteShift(req.params.id);
  res.status(httpStatus.OK).json({ status: 'success', message: 'Deleted success!' });
});

module.exports = {
  createShift,
  getAllShifts,

  updateShift,
  deleteShift,
};

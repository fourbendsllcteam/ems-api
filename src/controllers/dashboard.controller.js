const logger = require('../config/logger');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const dashboardService = require('../services/dashboard.service');

const getDashboardData = catchAsync(async (req, res) => {
  logger.info('dashboard.controller.js - inside getDashboardData');
  const availableCount = await dashboardService.getEmployeesCountByStatus(req.query.status);
  const otCount = await dashboardService.getOTEmployeesCount();
  res.status(httpStatus.OK).json({
    status: 'success',
    data: {
      availableCount,
      otCount,
    },
  });
});

const testRoute = catchAsync(async (req, res) => {
  logger.info('dashboard.controller.js - testRoute');
  let resData = 'Running on ' + process.env.NODE_ENV.toUpperCase() + ' environment...' + ' Testing OK';
  res.status(httpStatus.OK).json({
    status: 'success',
    data: {
      resData,
    },
  });
});

module.exports = { getDashboardData, testRoute };

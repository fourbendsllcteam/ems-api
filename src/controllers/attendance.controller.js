/* eslint-disable no-empty */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const attendanceService = require('../services/attendance.service');

const createAttendanceRegister = catchAsync(async (req, res) => {
  const attendanceRegister = await attendanceService.createAttendanceRegister(req.body);
  res.status(httpStatus.CREATED).json({ status: 'success', data: { attendanceRegister } });
});

const updateAttendanceRegister = catchAsync(async (req, res) => {
  const attendanceRegister = await attendanceService.updateAttendanceRegister(req.body);
  res.status(httpStatus.OK).json({ status: 'success', data: { attendanceRegister } });
});

const getPeriodWiseAttendance = catchAsync(async (req, res) => {
  const periodWiseAttendance = await attendanceService.getPeriodWiseAttendance();
  res.status(httpStatus.OK).json({
    status: 'success',
    result: periodWiseAttendance.length,
    data: {
      periodWiseAttendance,
    },
  });
});

const getEmployeeWiseAttendance = catchAsync(async (req, res) => {
  const employeeWiseAttendance = await attendanceService.getEmployeeWiseAttendance(req.params.empId);
  res.status(httpStatus.OK).json({
    status: 'success',
    result: employeeWiseAttendance.length,
    data: {
      employeeWiseAttendance,
    },
  });
});
module.exports = { createAttendanceRegister, updateAttendanceRegister, getPeriodWiseAttendance, getEmployeeWiseAttendance };

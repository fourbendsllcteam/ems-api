/* eslint-disable prettier/prettier */
const Joi = require('@hapi/joi');

const createAttendance = {
  body: Joi.object().keys({
    empId: Joi.string().required(),
  }),
};

const checkLogging = {
  body: Joi.object().keys({
    channelName: Joi.string().required(),
    channelId: Joi.string().required(),
  }),
};

module.exports = {
  createAttendance,
  checkLogging,
};

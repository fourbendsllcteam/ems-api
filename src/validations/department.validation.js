/* eslint-disable prettier/prettier */
const Joi = require('@hapi/joi');

const createDepartment = {
  body: Joi.object().keys({
    deptName: Joi.string().required(),
  }),
};

const updateDepartment = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
  body: Joi.object().keys({
    deptName: Joi.string().required(),
  }),
};

const deleteDepartment = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

module.exports = {
  createDepartment,
  updateDepartment,
  deleteDepartment,
};

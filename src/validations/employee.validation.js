/* eslint-disable prettier/prettier */
const Joi = require('@hapi/joi');

const createEmployee = {
  body: Joi.object().keys({
    companyEmpId: Joi.string().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    address: Joi.object().required(),
    email: Joi.string().optional(),
    phone: Joi.object().required(),
    empType: Joi.string().required(),
    department: Joi.string().required(),
    shift: Joi.string().required(),
    entryChannel: Joi.object().optional(),
  }),
};

const updateEmployee = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
  body: Joi.object().keys({
    companyEmpId: Joi.string().optional(),
    firstName: Joi.string().optional(),
    lastName: Joi.string().optional(),
    address: Joi.object().optional(),
    email: Joi.string().optional(),
    phone: Joi.object().optional(),
    empType: Joi.string().optional(),
    department: Joi.string().optional(),
    shift: Joi.string().optional(),
    entryChannel: Joi.object().optional(),
  }),
};

const updateShift = {
  body: Joi.object().keys({
    shift: Joi.string().optional(),
    employees: Joi.array().optional(),
    departments: Joi.array().optional(),
  }),
};

const deleteEmployee = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

const getEmployee = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

module.exports = {
  createEmployee,
  updateEmployee,
  deleteEmployee,
  getEmployee,
  updateShift,
};

/* eslint-disable prettier/prettier */
const Joi = require('@hapi/joi');

const createShift = {
  body: Joi.object().keys({
    shiftName: Joi.string().required(),
    startTime: Joi.string().required(),
    endTime: Joi.string().required(),
  }),
};

const updateShift = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
  body: Joi.object().keys({
    shiftName: Joi.string().optional(),
    startTime: Joi.string().optional(),
    endTime: Joi.string().optional(),
  }),
};

const deleteShift = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

module.exports = {
  createShift,
  updateShift,
  deleteShift,
};

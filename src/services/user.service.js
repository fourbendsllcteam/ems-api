const logger = require('../config/logger');
const httpStatus = require('http-status');
const EmsModel = require('../models/ems.model');
const ApiError = require('../utils/ApiError');

const PKAdminUserMaster = 'ADMIN_USER_MASTER';
const SKPrefixAdminUserMaster = '_ADMIN_USER_';

function createAdminUserImpl(userBody) {
  const user = new EmsModel({
    pk: PKAdminUserMaster,
    sk: SKPrefixAdminUserMaster + userBody.email,
    username: userBody.username,
    name: userBody.name,
    user_email: userBody.email,
    password: userBody.password,
    roles: 'admin',
  });
  user
    .save()
    .then((result) => {
      logger.debug(result);
    })
    .catch((err) => {
      logger.error(err);
    });
  return user;
}

const isEmailTaken = async (email) => {
  return EmsModel.get({ pk: PKAdminUserMaster, sk: SKPrefixAdminUserMaster + email });
};

const createUser = async (userBody) => {
  const existingUser = await isEmailTaken(userBody.email);
  if (existingUser) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  const user = await createAdminUserImpl(userBody);
  return user;
};

const getUserByUsername = async (username) => {
  return EmsModel.query('username').eq(username).using('user-index').exec();
};
const getUserByEmailImpl = async (email) => {
  return EmsModel.get({ pk: PKAdminUserMaster, sk: SKPrefixAdminUserMaster + email });
};

const getUserByEmail = async (email) => {
  const user = await getUserByEmailImpl(email);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
  return user;
};

module.exports = {
  createUser,
  getUserByUsername,
  getUserByEmail,
};

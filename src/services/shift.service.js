const httpStatus = require('http-status');
const EmsModel = require('../models/ems.model');
const ApiError = require('../utils/ApiError');
const { v4: uuidv4 } = require('uuid');
const employeeModel = require('../models/employee.model');
const moment = require('moment');
const Duration = require('duration');
const logger = require('../config/logger');

const PKShiftMaster = 'SHIFT_MASTER';
const SKPrefixShiftMaster = '_SHIFT_';

function createShiftImpl(data) {
  const shiftUUID = uuidv4();
  const startTimeFormatted = moment(data.startTime, 'hh:mm a').toDate();
  const endTimeFormatted = moment(data.endTime, 'hh:mm a').toDate();
  const shiftDuration = new Duration(new Date(startTimeFormatted), new Date(endTimeFormatted)).minutes;

  const shift = new EmsModel({
    pk: PKShiftMaster,
    sk: SKPrefixShiftMaster + shiftUUID,
    shift_id: shiftUUID,
    shift_name: data.shiftName,
    start_time: data.startTime,
    end_time: data.endTime,
    shift_duration: shiftDuration < 0 ? -shiftDuration : shiftDuration,
    shift_name_search_string: data.shiftName.toLowerCase().replace(/\s+/g, ''),
  });
  shift
    .save()
    .then((result) => {
      logger.debug(result);
    })
    .catch((err) => {
      logger.error(err);
    });
  return shift;
}

const getShiftById = async (id) => {
  return EmsModel.get({ pk: PKShiftMaster, sk: SKPrefixShiftMaster + id });
};

const getShiftByName = async (shiftName) => {
  const shift = await EmsModel.query('pk').eq(PKShiftMaster).filter('shift_name').eq(shiftName).exec();
  return shift;
};

const isShiftExist = async (shiftName) => {
  const shift = await EmsModel.query('pk')
    .eq(PKShiftMaster)
    .filter('shift_name_search_string')
    .eq(shiftName.toLowerCase().replace(/\s+/g, ''))
    .exec();
  if (shift.length) {
    return 0;
  }
  return 1;
};

const createShift = async (reqBody) => {
  const allShifts = await getAllShifts();
  logger.debug('all shifts, ', allShifts.length);

  /*   if (allShifts.length === 5) {
    throw new ApiError(httpStatus.INSUFFICIENT_STORAGE, 'More than 5 shifts not allowed');
  } else { */
  const existingShift = await isShiftExist(reqBody.shiftName);
  let shift;
  if (existingShift) {
    shift = await createShiftImpl(reqBody);
  } else {
    throw new ApiError(httpStatus.OK, 'Shift name already exists');
  }

  return shift;
};

const getAllShifts = async () => {
  const shifts = await EmsModel.query('pk').eq(PKShiftMaster).exec();
  return shifts;
};

const updateShift = async (shiftId, reqBody) => {
  const shift = await getShiftById(shiftId);
  if (!shift) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
  const startTimeFormatted = moment(reqBody.startTime, 'hh:mm a').toDate();
  const endTimeFormatted = moment(reqBody.endTime, 'hh:mm a').toDate();
  const shiftDuration = new Duration(new Date(startTimeFormatted), new Date(endTimeFormatted)).minutes;
  const data = {
    shift_name: reqBody.shiftName,
    start_time: reqBody.startTime,
    end_time: reqBody.endTime,
    shift_duration: shiftDuration < 0 ? -shiftDuration : shiftDuration,
    shift_name_search_string: reqBody.shiftName.toLowerCase().replace(/\s+/g, ''),
  };
  EmsModel.update({ pk: PKShiftMaster, sk: SKPrefixShiftMaster + shiftId }, data);
  const updatedShift = await getShiftById(shiftId);
  return updatedShift;
};

const deleteShift = async (shiftId) => {
  logger.debug('inside delete service', shiftId);
  const shift = await getShiftById(shiftId);
  let employees = '';
  if (!shift) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  } else {
    logger.debug('inside delete service else condn,', shift.shift_name);

    employees = await employeeModel.getEmployeesByShiftImpl(shift.shift_name);

    if (employees.length) {
      throw new ApiError(httpStatus.OK, 'Not able to delete since employee assigned to this shift');
    } else {
      EmsModel.delete({ pk: PKShiftMaster, sk: SKPrefixShiftMaster + shiftId });
    }
  }
};

module.exports = {
  createShift,
  getAllShifts,
  updateShift,
  deleteShift,
  getShiftById,
  getShiftByName,
};

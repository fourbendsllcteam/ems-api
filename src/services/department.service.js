const httpStatus = require('http-status');
const EmsModel = require('../models/ems.model');
const ApiError = require('../utils/ApiError');
const { v4: uuidv4 } = require('uuid');
const employeeService = require('./employee.service');
const logger = require('../config/logger');

const PKDepartmentMaster = 'DEPT_MASTER';
const SKPrefixDeparttmentMaster = '_DEPT_';

function createDepartmentImpl(data) {
  const departmentUUID = uuidv4();
  const dept = new EmsModel({
    pk: PKDepartmentMaster,
    sk: SKPrefixDeparttmentMaster + departmentUUID,
    dept_id: departmentUUID,
    dept_name: data.deptName,
    dept_name_search_string: data.deptName.toLowerCase().replace(/\s+/g, ''),
  });
  dept
    .save()
    .then((result) => {
      logger.debug(result);
    })
    .catch((err) => {
      logger.error(err);
    });
  return dept;
}

const getDeptById = async (id) => {
  return EmsModel.get({ pk: PKDepartmentMaster, sk: SKPrefixDeparttmentMaster + id });
};

const isDepartmentExist = async (deptName) => {
  const dept = await EmsModel.query('pk')
    .eq(PKDepartmentMaster)
    .filter('dept_name_search_string')
    .eq(deptName.toLowerCase().replace(/\s+/g, ''))
    .exec();
  if (dept.length) {
    return 0;
  }
  return 1;
};

const createDept = async (reqBody) => {
  const existingDepartment = await isDepartmentExist(reqBody.deptName);
  let dept;
  if (existingDepartment) {
    dept = await createDepartmentImpl(reqBody);
  } else {
    throw new ApiError(httpStatus.OK, 'Department name already exists');
  }
  return dept;
};

const getAllDepts = async () => {
  const depts = await EmsModel.query('pk').eq(PKDepartmentMaster).exec();
  return depts;
};

const updateDept = async (deptId, reqBody) => {
  const dept = await getDeptById(deptId);
  if (!dept) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  } else {
    const employees = await employeeService.getEmployeesByDepartment(dept.dept_name);
    if (employees.length) {
      throw new ApiError(httpStatus.OK, 'Not able to change department name since employee assigned to this department');
    } else {
      const data = {
        dept_name: reqBody.deptName,
        dept_name_search_string: reqBody.deptName.toLowerCase().replace(/\s+/g, ''),
      };
      const updatedDept = await EmsModel.update({ pk: PKDepartmentMaster, sk: SKPrefixDeparttmentMaster + deptId }, data);
      return updatedDept;
    }
  }
};

const deleteDept = async (deptId) => {
  const dept = await getDeptById(deptId);

  if (!dept) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  } else {
    const employees = await employeeService.getEmployeesByDepartment(dept.dept_name);
    if (employees.length) {
      throw new ApiError(httpStatus.OK, 'Not able to delete since employee assigned to this department');
    } else {
      EmsModel.delete({ pk: PKDepartmentMaster, sk: SKPrefixDeparttmentMaster + deptId });
    }
  }
};

module.exports = {
  createDept,
  getAllDepts,
  updateDept,
  deleteDept,
  getDeptById,
};

/* eslint-disable no-console */
const jwt = require('jsonwebtoken');
const moment = require('moment');
const httpStatus = require('http-status');
const config = require('../config/config');
const userService = require('./user.service');
const EmsModel = require('../models/ems.model');
const logger = require('../config/logger');

const ApiError = require('../utils/ApiError');
const { tokenTypes } = require('../config/tokens');
const { indexes } = require('../config/secondaryindex');

const generateToken = (userName, expires, type, secret = config.jwt.secret) => {
  const payload = {
    sub: userName,
    iat: moment().unix(),
    exp: expires.unix(),
    type,
  };
  return jwt.sign(payload, secret);
};

function createToken(token, userName, emailAdddress, expires, type, blacklisted) {
  const tokenDoc = new EmsModel({
    pk: 'TOKEN',
    sk: userName,
    token,
    user: userName,
    token_email: emailAdddress,
    expires,
    type,
    blacklisted,
  });
  tokenDoc
    .save()
    .then((result) => {
      logger.debug(result);
    })
    .catch((err) => {
      logger.error(err);
    });
  return tokenDoc;
}

const saveToken = async (token, userName, email, expires, type, blacklisted = false) => {
  const tokenDoc = await createToken(token, userName, email, expires, type, blacklisted);
  return tokenDoc;
};

const verifyToken = async (token, type) => {
  const payload = jwt.verify(token, config.jwt.secret);
  const tokenDoc = await EmsModel.query('token')
    .eq(token)
    .and()
    .filter('type')
    .eq(type)
    .filter('user')
    .eq(payload.sub)
    .filter('blacklisted')
    .eq(false)
    .using(indexes.TOKEN_INDEX)
    .exec();

  // Token.findOne({ token, type, user: payload.sub, blacklisted: false });
  if (tokenDoc.length) {
    if (!tokenDoc) {
      throw new Error('Token not found');
    }
  } else {
    throw new Error('Token not found');
  }

  return tokenDoc[0];
};

const generateAuthTokens = async (user) => {
  const accessTokenExpires = moment().add(config.jwt.accessExpirationMinutes, 'minutes');
  const accessToken = generateToken(user.username, accessTokenExpires, tokenTypes.ACCESS);

  const refreshTokenExpires = moment().add(config.jwt.refreshExpirationDays, 'days');
  const refreshToken = generateToken(user.username, refreshTokenExpires, tokenTypes.REFRESH);
  await saveToken(refreshToken, user.username, user.user_email, refreshTokenExpires.toDate().toString(), tokenTypes.REFRESH);

  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.toDate(),
    },
    refresh: {
      token: refreshToken,
      expires: refreshTokenExpires.toDate(),
    },
  };
};

module.exports = {
  saveToken,
  verifyToken,
  generateAuthTokens,
};

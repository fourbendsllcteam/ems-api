const logger = require('../config/logger');
const httpStatus = require('http-status');
const tokenService = require('./token.service');
const userService = require('./user.service');
const ApiError = require('../utils/ApiError');
const EmsModel = require('../models/ems.model');
const { tokenTypes } = require('../config/tokens');
const { indexes } = require('../config/secondaryindex');

const PKTokenMaster = 'TOKEN';

const loginUserWithEmailAndPassword = async (username, password) => {
  const user = await userService.getUserByUsername(username);
  logger.debug('password received: ', user[0]);
  if (user.length) {
    if (user[0].password !== password) {
      throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
    }
  } else {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
  }
  return user[0];
};

const logout = async (refreshToken) => {
  const refreshTokenDoc = await EmsModel.query('token')
    .eq(refreshToken)
    .and()
    .filter('type')
    .eq(tokenTypes.REFRESH)
    .filter('blacklisted')
    .eq(false)
    .using(indexes.TOKEN_INDEX)
    .exec();

  if (refreshTokenDoc.length) {
    if (!refreshTokenDoc) {
      throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
    }
  } else {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Not found');
  }

  logger.debug('refreshToken', refreshTokenDoc);
  await EmsModel.delete({ pk: PKTokenMaster, sk: refreshTokenDoc[0].user });
};

const refreshAuth = async (refreshToken) => {
  try {
    const refreshTokenDoc = await tokenService.verifyToken(refreshToken, tokenTypes.REFRESH);
    logger.debug('refreshTokenDoc', refreshTokenDoc);
    const user = await userService.getUserByUsername(refreshTokenDoc.user);
    logger.debug('user', user[0]);
    if (user.length) {
      if (!user) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
      }
    } else {
      throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
    }
    await EmsModel.delete({ pk: PKTokenMaster, sk: refreshTokenDoc.user });
    return tokenService.generateAuthTokens(user[0]);
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Please authenticate');
  }
};

module.exports = {
  loginUserWithEmailAndPassword,
  logout,
  refreshAuth,
};

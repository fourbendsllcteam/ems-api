const httpStatus = require('http-status');
const EmsModel = require('../models/ems.model');
const ApiError = require('../utils/ApiError');
const { v4: uuidv4 } = require('uuid');
const attendanceModel = require('../models/attendance.model');

const getEmployeesCountByStatus = async (status) => {
  const count = await attendanceModel.getEmployeesCountByStatusImpl(status);
  return count;
};

const getOTEmployeesCount = async (status) => {
  const count = await attendanceModel.getOTEmployeesCountImpl();
  return count;
};

module.exports = {
  getEmployeesCountByStatus,
  getOTEmployeesCount,
};

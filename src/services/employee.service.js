const httpStatus = require('http-status');
const EmsModel = require('../models/ems.model');
const ApiError = require('../utils/ApiError');
const moment = require('moment');
const Duration = require('duration');
const employeeModel = require('../models/employee.model');
const entryChannelModel = require('../models/entry_channel.model');
const shiftService = require('./shift.service');
const logger = require('../config/logger');

const createEmployee = async (reqBody) => {
  const existenceCheck = await employeeModel.getEmployeesByCompanyEmpIdImpl(reqBody.companyEmpId);

  if (!existenceCheck.length == 0) {
    throw new ApiError(httpStatus.OK, 'EmployeeID already exists');
  }

  const employee = await employeeModel.createEmployeeImpl(reqBody);
  logger.debug('emp_id', employee.Item.emp_id);

  if (reqBody.entryChannel) {
    logger.debug('going to updateEntryChannel');
    await updateEntryChannel(employee.Item.emp_id, reqBody);
  }

  return employee;
};

const getAllEmployees = async () => {
  const employees = await employeeModel.getAllEmployeesImpl();
  return employees;
};

const getEmployeesByName = async (param) => {
  const employees = await employeeModel.getEmployeesByNameImpl(param);
  return employees;
};

const getEmployeesByDepartment = async (param) => {
  const employees = await employeeModel.getEmployeesByDepartmentImpl(param);
  return employees;
};

const getEmployeesByShift = async (param) => {
  const employees = await employeeModel.getEmployeesByShiftImpl(param);
  return employees;
};

const getEmployeesOfCurrentShift = async (ts) => {
  logger.info('inside getEmployeesOfCurrentShift: ');
  let allShifts = await shiftService.getAllShifts();
  let m = moment();

  logger.debug('allshifts: ', allShifts);

  let startTimeFormatted = moment();
  let endTimeFormatted = moment();
  let currentShiftId = [];
  let totalEmployeesCount = 0;

  logger.debug('m: ', m.toDate());
  logger.debug('ts: ', ts);

  m = ts === 'api' ? m.toDate() : moment(ts, 'hh:mm a').toDate();

  for (let val of allShifts) {
    startTimeFormatted = moment(val.start_time, 'hh:mm a').toDate();
    endTimeFormatted = moment(val.end_time, 'hh:mm a').toDate();
    let shiftDuration = new Duration(new Date(startTimeFormatted), new Date(endTimeFormatted)).minutes;
    logger.debug(val.shift_name, shiftDuration);
    logger.debug('before updating: ', endTimeFormatted);
    endTimeFormatted = shiftDuration < 0 ? moment(val.end_time, 'hh:mm a').add(1, 'days').toDate() : endTimeFormatted;
    logger.debug('after updating: ', endTimeFormatted);

    if (m > startTimeFormatted && m <= endTimeFormatted) {
      logger.debug('current shift found, it is: ', val.shift_name);
      currentShiftId.push(val.shift_name);
    }
  }
  logger.debug('loop ended, there are ', currentShiftId.length);
  for (let eachShift of currentShiftId) {
    logger.debug('each shift: ', eachShift);
    let employees = await getEmployeesByShift(eachShift);
    logger.debug('employees count', employees.length);
    totalEmployeesCount += employees.length;
  }
  logger.debug('totalEmployeesCount: ', totalEmployeesCount);
  return totalEmployeesCount;
};

const getEmployeeByChannel = async (channelName, channelId) => {
  const result = await employeeModel.getEmployeeByChannelImpl(channelName, channelId);
  //  logger.debug('result: ', result);
  const employee = await getEmployee(result[0].emp_id);
  return employee;
};

const updateEmployee = async (empId, reqBody) => {
  await employeeModel.updateEmployeeImpl(empId, reqBody);
  if (reqBody.entryChannel) {
    logger.info('going to updateEntryChannel');
    await updateEntryChannel(empId, reqBody);
  }
};

const updateShiftByDepartment = async (departmentName, reqBody) => {
  await employeeModel.updateShiftByDepartmentImpl(departmentName, reqBody);
};

const updateEntryChannel = async (empId, reqBody) => {
  logger.info('inside updateEntryChannel');
  const employee = await employeeModel.getEmployeeByIdImpl(empId);
  if (!employee) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }

  // taking current all entry channels of employee
  let entryChannel = employee.entry_channel;

  logger.debug('current entry channel: ', JSON.stringify(reqBody.entryChannel));
  const entryChannelItem = reqBody.entryChannel;
  let entryChannelName;
  let entryChannelValue;
  let key;
  for (key in entryChannelItem) {
    if (entryChannelItem.hasOwnProperty(key)) {
      entryChannelName = key;
      entryChannelValue = entryChannelItem[key];
      logger.debug('channel name: ', entryChannelName + ' value: ' + entryChannelValue);
    }
    entryChannel[entryChannelName] = entryChannelValue;
    // creating entry channel employee as seperate entry
    const params = { channelName: entryChannelName, channelId: entryChannelValue, entryChannelEmpId: empId };

    if (!entryChannelValue == '') {
      logger.info('entry channel has not null value');
      await entryChannelModel.createEntryChannelEmployeeImpl(params);
    }
  }
  // logger.debug('updated entry channel: ', entryChannel);
  await employeeModel.updateEntryChannelImpl(empId, entryChannel);
};

const deleteEmployee = async (empId) => {
  await employeeModel.deleteEmployeeImpl(empId);
};

const getEmployee = async (empId) => {
  const employee = await employeeModel.getEmployeeByIdImpl(empId);
  if (!employee) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
  //  logger.debug('empid: ', employee);
  return employee;
};

const getEmployeeByCompanyEmpId = async (companyEmpId) => {
  const employee = await employeeModel.getEmployeesByCompanyEmpIdImpl(companyEmpId);
  if (!employee) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
  //  logger.debug('empid: ', employee);
  return employee;
};

module.exports = {
  createEmployee,
  getAllEmployees,
  updateEmployee,
  deleteEmployee,
  getEmployee,
  getEmployeesByName,
  getEmployeesByDepartment,
  getEmployeesByShift,
  getEmployeeByChannel,
  updateEntryChannel,
  updateShiftByDepartment,
  getEmployeeByCompanyEmpId,
  getEmployeesOfCurrentShift,
};

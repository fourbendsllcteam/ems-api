const httpStatus = require('http-status');
const EmsModel = require('../models/ems.model');
const ApiError = require('../utils/ApiError');
const employeeService = require('../services/employee.service');
const shiftService = require('../services/shift.service');
const moment = require('moment');
const Duration = require('duration');
const logger = require('../config/logger');
const PKEmpAttnMaster = 'EMP_ATTN_';

const PKPeriodAttn = 'PERIOD_ATTN';

function closeOldAttendance(pkValue, skValue) {
  EmsModel.update(
    { pk: pkValue, sk: skValue },
    {
      $SET: {
        attn_reg_status: 'CLOSED',
      },
    },
    (error, result) => {
      if (error) {
        console.error(error);
      } else {
        logger.debug(result);
      }
    }
  );
}

function createAttendanceRegisterImpl(data, shiftDuration) {
  let m = moment();
  let attnRegId = m.format('MM').toString() + m.format('yyyy').toString() + m.format('DD').toString();
  if (data.current_day === attnRegId) {
    m.add(1, 'days').calendar();
    attnRegId = m.format('MM').toString() + m.format('yyyy').toString() + m.format('DD').toString();
  }
  const attendanceRegister = new EmsModel({
    pk: PKEmpAttnMaster + data.emp_id,
    sk: attnRegId,
    attn_date: m.format('ddd MMM Do, YYYY'),
    period_attendance: PKPeriodAttn,
    emp_name: data.first_name + ' ' + data.last_name,
    emp_id: data.emp_id,
    company_emp_id: data.company_emp_id,
    dept_name: data.department,
    shift_name: data.shift,
    entry_logs: [],
    entry_channel: [],
    exit_logs: [],
    exit_channel: [],
    duration: 0,
    ot_hours: 0,
    lacked_hours: 0,
    current_status: 'OUT',
    first_in: '',
    last_out: '',
    att_reg_status: 'OPEN',
    ideal_duration: shiftDuration,
  });
  attendanceRegister
    .save()
    .then((result) => {
      logger.debug(result);
    })
    .catch((err) => {
      logger.error(err);
    });
  return attendanceRegister;
}

const createAttendanceRegister = async (reqBody) => {
  let employee = await employeeService.getEmployee(reqBody.empId);
  const currentShift = await shiftService.getShiftByName(employee.shift);
  const shiftDuration = currentShift[0].shift_duration;
  const attendanceRegister = await createAttendanceRegisterImpl(employee, shiftDuration);
  const employeeAttribute = { currentDay: attendanceRegister.sk };
  employee = await employeeService.updateEmployee(reqBody.empId, employeeAttribute);
  return attendanceRegister;
};
const getRegisterById = async (empId, currentDay) => {
  return EmsModel.get({ pk: empId, sk: currentDay });
};

/*
 * - firstly we need to get employee by channel name and channel id
 * - if employee not found we throwing error
 * - else employee found, we need to current_day value from employee attribute
 * - if current_day value is null, then it is understood, he is a new employee and have to create new attendance register and then entry will be updated to that newly created register
 * - else current_day has value means, we will go for checking isNextDay case
 * - if it is a next day, have to create new attendance register and then entry will be updated to that newly created register
 * - else it is not next day means, we will get current register id by using current_day value and then entry will be updated to existing regiser
 * - then calculating duration will take place
 * - then first_in value will be assigned if it is first in.
 * - finally entry or exit logs will be updated based on current_status
 */

const updateAttendanceRegister = async (reqBody) => {
  logger.info('ATTENDANCE LOGGING');
  let employeeFoundCase = 'IT IS A EMPLOYEE FOUND CASE: ';
  let newEmployeeCase = 'IT IS NOT A NEW EMPLOYEE CASE: ';
  let nextDayCase = 'IT IS NOT A NEXT DAY CASE: ';
  let firstInCase = 'IT IS NOT A FIRST IN CASE: ';
  let m = moment();
  let channelName = reqBody.channelName;
  let channelId = reqBody.channelId;

  let attendanceRegister = '';
  let lastAttnDate = '';

  //getting employee by channel name and channel id
  let employee = await employeeService.getEmployeeByChannel(channelName, channelId);
  logger.info('CHECKING - EMPLOYEE FOUND CASE');
  //throwing error if not found
  if (!employee) {
    employeeFoundCase = 'EMPLOYEE NOT FOUND CASE: ';
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }

  logger.info(employeeFoundCase);

  logger.info('CHECKING - NEW EMPLOYEE CASE: ' + employee.current_day);
  const empId = employee.emp_id;
  let currentDay = employee.current_day;

  const currentShift = await shiftService.getShiftByName(employee.shift);
  const shiftDuration = currentShift[0].shift_duration;

  // if current day is null, then the employee is considered as new employee, so need to create attendance register
  if (employee.current_day === null || employee.current_day === '') {
    newEmployeeCase = 'IT IS A NEW EMPLOYEE CASE: ';

    attendanceRegister = await createAttendanceRegisterImpl(employee, shiftDuration);

    const employeeAttribute = { currentDay: attendanceRegister.sk };
    await employeeService.updateEmployee(empId, employeeAttribute);
    employee = await employeeService.getEmployee(empId);
  } else {
    attendanceRegister = await getRegisterById(PKEmpAttnMaster + empId, currentDay);
    lastAttnDate = attendanceRegister.attn_date;
  }
  logger.info(newEmployeeCase);

  logger.info('CHECKING - NEXT DAY CASE: ');

  logger.info('lastAttnDate: ', lastAttnDate);
  // for duration calculation we getting and comparing shift close time and current timestamp

  const shiftStartTimeString = currentShift[0].start_time;
  logger.debug('shiftStartTimeString: ', shiftStartTimeString);
  logger.debug('concatenation: ', lastAttnDate + ' ' + shiftStartTimeString);

  const currentTimeString = m.format('hh:mm a').toString();
  const shiftStartTimeStringFormat = moment(
    lastAttnDate || m.format('ddd MMM Do, YYYY') + ' ' + shiftStartTimeString,
    'ddd MMM Do, YYYY hh:mm a'
  ).toDate();
  const currentTimeStringFormat = moment(currentTimeString, 'hh:mm a').toDate();

  logger.debug('shiftStartTimeFormat: ', shiftStartTimeStringFormat);
  logger.debug('currentTimeFormat: ', currentTimeStringFormat);

  let isNextDay = new Duration(new Date(shiftStartTimeStringFormat), new Date(currentTimeStringFormat)).minutes;
  logger.debug('shiftChangeDifferenceValue: ', isNextDay);

  /// isNextDay is greather than or equal to 1200 means, we have to create new attendance register and save the entry to that record
  if (isNextDay >= 1200) {
    nextDayCase = 'IT IS A NEXT DAY CASE: ';
    // closing old attendance before creating new
    await closeOldAttendance(PKEmpAttnMaster + empId, currentDay);
    attendanceRegister = await createAttendanceRegisterImpl(employee, shiftDuration);
    const employeeAttribute = { currentDay: attendanceRegister.sk };
    await employeeService.updateEmployee(empId, employeeAttribute);
    employee = await employeeService.getEmployee(empId);
  }

  logger.info(nextDayCase);

  // getting and assigning required param
  logger.debug('current_day: ' + employee.current_day);
  currentDay = employee.current_day;

  const pkValue = PKEmpAttnMaster + empId;
  // assigning attendance register if it is not under new employee case and isNextDay case
  attendanceRegister = await getRegisterById(pkValue, currentDay);

  let currentStatus;
  logger.info('CALCULATING DURATION');
  // calculating total duration
  let totalDuration = attendanceRegister.duration;
  let lastIn = attendanceRegister.entry_logs[attendanceRegister.entry_logs.length - 1];
  logger.debug('length' + attendanceRegister.entry_logs.length);
  logger.debug('lastin: ' + lastIn);

  // exit logs
  if (attendanceRegister.current_status === 'IN') {
    logger.info('EXIT LOG CASE');

    currentStatus = 'OUT';
    let duration = new Duration(new Date(lastIn), new Date(m.toString())).minutes;
    logger.info('duration: ' + duration);

    //calculating lacked hours
    logger.info('CALCULATING LACKED HOURS');
    let lackedHours = shiftDuration - (totalDuration + duration);

    if (lackedHours < 0) {
      lackedHours = 0;
    }

    logger.info('LACKED HOURS: ' + lackedHours);
    //calculating lacked hours
    logger.info('CALCULATING OT HOURS');
    let otHours = totalDuration + duration - shiftDuration;
    if (otHours < 0) {
      otHours = 0;
    }
    logger.info('OT HOURS: ' + otHours);

    EmsModel.update(
      { pk: pkValue, sk: currentDay },
      {
        $ADD: {
          exit_logs: [m.toString()],
          exit_channel: [reqBody.channelName],
        },
        $SET: {
          current_status: currentStatus,
          last_out: m.toString(),
          duration: totalDuration + duration,
          lacked_hours: lackedHours,
          ot_hours: otHours,
        },
      },
      (error, result) => {
        if (error) {
          console.error(error);
        } else {
          logger.debug(result);
        }
      }
    );
  }
  // entry logs
  // in entry logs we set last_out value as '-'
  else {
    logger.info('ENTRY LOG CASE');

    logger.info('CEHCKING - FIRST IN CASE');
    // setting first in, if it is the first entry for that day
    let firstIn = attendanceRegister.first_in || '';
    if (attendanceRegister.entry_logs.length === 0) {
      firstInCase = 'IT IS A FIRST IN CASE';
      firstIn = m.toString();
    }

    logger.info(firstInCase);

    currentStatus = 'IN';
    EmsModel.update(
      { pk: pkValue, sk: currentDay },
      {
        $ADD: {
          entry_logs: [m.toString()],
          entry_channel: [reqBody.channelName],
        },
        $SET: {
          current_status: currentStatus,
          first_in: firstIn,
          last_out: '-',
        },
      },
      (error, result) => {
        if (error) {
          console.error(error);
        } else {
          logger.debug(result);
        }
      }
    );
  }

  return attendanceRegister;
};

const getPeriodWiseAttendance = async () => {
  const periodWiseAttendance = await EmsModel.scan().using('period-attendance').exec();
  return periodWiseAttendance;
};

const getEmployeeWiseAttendance = async (empId) => {
  let m = moment();
  const month = m.format('MM').toString() + m.format('yyyy').toString();
  logger.debug('input: month, year and emp_id:  ', month + ' | ' + empId);
  const employeeWiseAttendance = await EmsModel.query('pk')
    .eq(PKEmpAttnMaster + empId)
    .and()
    .where('sk')
    .beginsWith(month)
    .sort('descending')
    .exec();
  return employeeWiseAttendance;
};

module.exports = { createAttendanceRegister, updateAttendanceRegister, getPeriodWiseAttendance, getEmployeeWiseAttendance };

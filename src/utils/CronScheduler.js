const CronJob = require('cron').CronJob;
const logger = require('../config/logger');

const val = '27 17 * * *';
var job = new CronJob(
  val,
  function () {
    logger.info('You will see this message every second');
  },
  null,
  true
);
job.start();

const { v4: uuidv4 } = require('uuid');

logger.debug('test: ', uuidv4());

const dynamoose = require('dynamoose');
const app = require('./app');
const config = require('./config/config');
const logger = require('./config/logger');

logger.info('---------------------------------------------------');
logger.info('Running on ' + process.env.NODE_ENV.toUpperCase() + ' environment...');

let server = '';

/* mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
  logger.info('Connected to MongoDB');
  server = app.listen(config.port, () => {
    logger.info(`Listening to port ${config.port}`);
  });
}); */

server = app.listen(config.port, () => {
  logger.info(`API Server started, Listening to the port ${config.port}`);
  logger.info('---------------------------------------------------');
});

dynamoose.aws.sdk.config = config.awsConfig;

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});

const dotenv = require('dotenv');
const path = require('path');
const Joi = require('@hapi/joi');

if (process.env.NODE_ENV.toUpperCase() === 'PRODUCTION') {
  dotenv.config({ path: path.join(__dirname, '../../production.env') });
} else if (process.env.NODE_ENV.toUpperCase() === 'LOCAL') {
  dotenv.config({ path: path.join(__dirname, '../../local.env') });
}

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi.string().valid('production', 'development', 'local', 'test').required(),
    PORT: Joi.number().default(3000),
    // MONGODB_URL: Joi.string().required().description('Mongo DB url'),
    JWT_SECRET: Joi.string().required().description('JWT secret key'),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number().default(30).description('minutes after which access tokens expire'),
    JWT_REFRESH_EXPIRATION_DAYS: Joi.number().default(30).description('days after which refresh tokens expire'),
    SMTP_HOST: Joi.string().description('server that will send the emails'),
    SMTP_PORT: Joi.number().description('port to connect to the email server'),
    SMTP_USERNAME: Joi.string().description('username for email server'),
    SMTP_PASSWORD: Joi.string().description('password for email server'),
    EMAIL_FROM: Joi.string().description('the from field in the emails sent by the app'),
    AWS_DB_REGION: Joi.string().required().description('AWS DB Region'),
    AWS_DB_ACCESS_KEY: Joi.string().required().description('AWS DB Access Key'),
    AWS_DB_SECRET_KEY: Joi.string().required().description('AWS DB Secret Key'),
    AWS_DB_ENDPOINT: Joi.string().optional().description('AWS DB End Point'),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: 'key' } }).validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  jwt: {
    secret: envVars.JWT_SECRET,
    accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: 10,
  },
  email: {
    smtp: {
      host: envVars.SMTP_HOST,
      port: envVars.SMTP_PORT,
      auth: {
        user: envVars.SMTP_USERNAME,
        pass: envVars.SMTP_PASSWORD,
      },
    },
    from: envVars.EMAIL_FROM,
  },
  awsConfig: {
    region: envVars.AWS_DB_REGION,
    accessKeyId: envVars.AWS_DB_ACCESS_KEY,
    secretAccessKey: envVars.AWS_DB_SECRET_KEY,
    endpoint: envVars.AWS_DB_ENDPOINT,
  },
};

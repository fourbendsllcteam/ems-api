const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const config = require('./config');
const { combine, timestamp, label, printf } = winston.format;
const moment = require('moment');

const ts = moment().format('YYYY-MM-DD hh:mm:ss');

const enumerateErrorFormat = winston.format((info) => {
  if (info instanceof Error) {
    Object.assign(info, { message: info.stack });
  }

  return info;
});

var fileRotateTransport = new DailyRotateFile({
  filename: '%DATE%.log',
  datePattern: 'DDMMYYYY',
  maxSize: '20m',
  maxFiles: '14d',
  dirname: 'logs',
  zippedArchive: true,
});

const logger = winston.createLogger({
  level: config.env === 'development' ? 'debug' : 'info',
  format: winston.format.combine(
    enumerateErrorFormat(),
    config.env === 'development' ? winston.format.colorize() : winston.format.uncolorize(),
    winston.format.splat(),
    winston.format.printf(({ level, message }) => `${ts} - ${level}: ${message}`)
  ),
  transports: [
    fileRotateTransport,
    new winston.transports.Console({
      format: winston.format.splat(),
    }),
  ],
});

if (process.env.NODE_ENV === 'local') {
logger.add(
  new winston.transports.Console({
    format: winston.format.splat(),
  })
); 


module.exports = logger;

/* eslint-disable prettier/prettier */
const indexes = {
  SHIFT_EMPLOYEE: 'shift-employee',
  USER_INDEX: 'user-index',
  TOKEN_INDEX: 'token-index',
  DEPARTMENT_EMPLOYEE: 'department-employee',
  CHANNEL_EMPLOYEE: 'channel-employee',
  INVERTED_INDEX: 'inverted-index',
  PERIOD_ATTENDANCE: 'period-attendance',
  STATUS_ATTENDANCE: 'status-attendance',
  COMPANY_EMP_ID_INDEX: 'id-employee',
};

module.exports = {
  indexes,
};

/* eslint-disable prettier/prettier */
const channels = {
  QR: 'ENTRY_CHANNEL_QR',
  RFID: 'ENTRY_CHANNEL_RFID',
  THUMB_READER: 'ENTRY_CHANNEL_TR',
  FACE_RECOGNITION: 'ENTRY_CHANNEL_FR',
  NFC: 'ENTRY_CHANNEL_NFC',
  BARCODE: 'ENTRY_CHANNEL_BC',
};

module.exports = {
  channels,
};
